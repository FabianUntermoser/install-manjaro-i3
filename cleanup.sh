#!/usr/bin/env bash

# remove apps
sudo pacman --remove --recursive --nosave --unneeded --noconfirm \
    palemoon-bin \
    morc_menu \
    moc \
    1> /dev/null

# Remove orphaned packages
sudo pacman --remove --recursive --nosave --noconfirm \
    "$(pacman --query --deps --unrequired --quiet)" \
    1> /dev/null

# Remove stale snaps
[ "$(command -v snapd)" ] && snap list --all | awk '/disabled/{print $1, $3}' |
    while read -r snapname revision; do
        sudo snap remove "$snapname" --revision="$revision"
    done
