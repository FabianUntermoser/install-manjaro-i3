#!/usr/bin/env bash

arch_packages=(
    nwg-displays
)

aur_packages=(
    espanso-wayland # cross plattform text expander
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done
