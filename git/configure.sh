#!/usr/bin/env bash

# simple git configuration
echo "Configuring git ..."
git config --global credential.helper store
git config --global core.fileMode false

# git push will now also push annotated and reachable tags
git config --global push.followTags true
