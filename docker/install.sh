#!/usr/bin/env bash

# Install packages
sudo pacman --sync --needed --noconfirm \
    docker \
    docker-compose
