#!/usr/bin/env bash

# Allow docker without root rights
sudo groupadd docker
sudo usermod --append --groups docker $USER
newgrp docker

sudo chown --recursive "$USER":"$USER" /home/"$USER"/.docker
sudo chmod --recursive g+rwx "$HOME/.docker"

# Start on boot
sudo systemctl enable docker
