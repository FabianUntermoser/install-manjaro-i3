#!/usr/bin/env bash

git clone https://gitlab.com/FabianUntermoser/st temp
cd temp || exit

sudo make clean install

cd "$OLDPWD" || exit

rm -rf temp
