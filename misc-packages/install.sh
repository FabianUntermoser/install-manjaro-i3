#!/usr/bin/env bash

arch_packages=(
    nextcloud-client
    keepassxc
    firefox
    dnsutils
    whois
    redshift
    zathura zathura-pdf-mupdf
    tk
    signal-desktop
    exa
    bat
    ueberzug # terminal imgage previewer
    sxiv
    xcape # for remapping single <Caps> to <Esc>
    xorg-xev # print keyboard codes
    xournalpp
    libnotify # notification support
    newsboat # rss client
    otf-font-awesome # font with glyphs
    obs-studio
    aspell aspell-en aspell-de # spellchecking
    mpv
    playerctl # control music players from the terminal
    udiskie # automount removable media
    bleachbit # clean up disk space
    baobab # disk usage analyzer
    blueman # bluetooth
    zerotier-one # like vpn
    zbar # for zbarimage (qr code scanner)
    obsidian # note taking app
    fcitx fcitx-configtool fcitx-mozc # japanese type system

    # audio
    pavucontrol # audio control

    ## ARCHIVE
    # xf86-input-wacom # wacom tablet drivers
    # inkscape
    # wireshark-cli wireshark-qt
    # anki
    # tldr # unix command cheatsheats
    # unoconv # convert libreoffice documents
    # asciinema # record terminal sessions
    # octave # matlab alternative
    # octave-control # octave-forge package for Computer-Aided Control System Design
)

aur_packages=(
    polybar-git # fixes workspaces not switching bug
    syncthingtray
    thunderbird-beta-bin
    cryptomator
    zotero
    caffeine-ng
    ttf-ms-fonts
    firefox-tridactyl-native # firefox vim addon
    nodejs-nativefier # websites as desktop applications
    flameshot-git # screenshotting tool
    openvpn-update-systemd-resolved # fix openvpn dns issue due to systemd
    obs-websocket-bin # control obs with obs-cli (https://github.com/leafac/obs-cli)
    spotify
    input-leap-git # fork from barrier. Open Source KVM switcher
    youtube-dl
    rustdesk-bin # teamviewer alternative
    zaread-git # docx reader

    ## ARCHIVE
    # discord
    # synology-drive
    # cheat # unix command cheatcheats
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done
