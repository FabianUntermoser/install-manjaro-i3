#!/usr/bin/env bash

# Install texlive
sudo pacman --sync --needed --noconfirm \
    texlive-core \
    texlive-binextra \
    texlive-langextra \
    texlive-science \
    biber \
    texlive-latexrecommended \
    texlive-latexextra \
    texlive-plaingeneric \
    texlive-bibtexextra \
    1> /dev/null


# install tllocalmgr for managing tex packages
# yay --sync --needed --noconfirm --norebuild --noredownload \
#     texlive-localmanager \
#     1> /dev/null
