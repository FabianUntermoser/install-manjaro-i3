#!/usr/bin/env bash

: ${XDG_CONFIG_HOME:="$HOME/.config"}

arch_packages=(
    # neofetch
    python-pywal
    ttf-jetbrains-mono
)

aur_packages=(
    # urxvt-font-size-git
    nerd-fonts-ubuntu-mono
    nerd-fonts-fira-code
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done

# Run wal to generate cache files
wal -i "/usr/share/backgrounds/"

# Install base16-shell
BASE16_DIR="$XDG_CONFIG_HOME/base16-shell"
git clone https://github.com/chriskempson/base16-shell.git "$BASE16_DIR"
