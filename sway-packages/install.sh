#!/usr/bin/env bash

arch_packages=(
    interception-dual-function-keys # remap capslock
    pipewire-pulse # for sway
    wireplumber # persist audio profiles
)

aur_packages=(
    nwg-displays # xrandr gui
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done
