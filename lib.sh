#!/usr/bin/env bash

log() {
    local text=$1
    command -v "figlet" >/dev/null && text=$(figlet "$text")
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    printf "\n${RED}%s${NC}\n\n" "$text"
}

ensure_privileges() {
    echo "ensuring priviliges are set for sudo commands"
    sudo chmod a+x --recursive .
}
