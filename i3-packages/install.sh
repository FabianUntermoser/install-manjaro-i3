#!/usr/bin/env bash

arch_packages=(
    perl-anyevent-i3 # i3-save-tree dependency
    perl-switch # dependency for i3-save-tree
    arandr # gui for xrandr
    pulseaudio # for audio
)

aur_packages=(
    i3-battery-popup-git
    xbanish # hide mouse when typing
    espanso-bin # cross plattform text expander
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done
