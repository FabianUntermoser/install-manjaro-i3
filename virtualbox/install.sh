#!/usr/bin/env bash

# https://wiki.manjaro.org/index.php?title=Virtualbox#Install_VirtualBox
VIRTUALBOX_KERNEL_PACKAGE=$(pacman -Qsq "^linux" | grep "^linux[0-9]*[-rt]*$" | awk '{print $1"-virtualbox-host-modules"}' ORS=' ' | tr -d '[:space:]')

# Install packages
sudo pacman --sync --needed --noconfirm \
    virtualbox \
    virtualbox-guest-iso \
    "$VIRTUALBOX_KERNEL_PACKAGE" \
    1> /dev/null

# enable virtualbox
sudo vboxreload

# add vboxusers group to enable all extension pack features
sudo usermod --append --groups vboxusers "$USER"
