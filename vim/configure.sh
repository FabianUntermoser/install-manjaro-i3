#!/usr/bin/env bash

# install plugins defined in ~/.vimrc with Vundle
vim +PlugUpdate +qall
