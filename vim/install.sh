#!/usr/bin/env bash

# Install vim (gvim for enabling +/* buffers for clipboard/primary selection)
sudo pacman --remove --recursive --nosave --unneeded --noconfirm vim > /dev/null
sudo pacman --sync --needed --noconfirm gvim 1> /dev/null

# remove plugin folder
rm -rf ~/.vim/bundle

# install vim-plug for plugin management
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install german spellfile
[ -f "/usr/share/nvim/runtime/spell/de.utf-8.spl" ] || {
    cd /usr/share/nvim/runtime/spell &&
        sudo wget http://ftp.vim.org/vim/runtime/spell/de.utf-8.spl
}
