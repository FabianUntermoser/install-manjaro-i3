#!/usr/bin/env bash

# libreoffice and spell checker
sudo pacman --sync --needed --noconfirm \
    libreoffice-fresh \
    hunspell-en_US hunspell-de \
    1> /dev/null
