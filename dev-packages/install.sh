#!/usr/bin/env bash

arch_packages=(
    rofi
    alacritty
    fzf
    fasd
    stow
    ranger # terminal file manager
    trash-cli
    entr # run cmd on file changes

    # tools
    jq
    xarchiver zip unrar unzip p7zip
    make
    npm

    # misc
    # filezilla
    # dbeaver

    httpie

    # shell
    shellcheck

    # c
    # clang
    # gdb
    # valgrind
    # ctags

    # java
    # maven
    # jdk-openjdk
    # icedtea-web # for opening .jnpl files (java web) #

    # php
    # composer

    # python
    # python2-bin
    # python

    # others
    # yarn
    # go

    # code

)

aur_packages=(
    ranger_devicons-git
    git-extras
    # insomnia-bin
    dragon-drop
    # nodejs-live-server
    pandoc-bin # pandoc without haskel dependencies
    jetbrains-toolbox
    # robo3t-bin # mongodb
)

# Install packages
for package in "${arch_packages[@]}"; do
    sudo pacman --sync --needed --noconfirm "$package" 1> /dev/null
done

# Install packages from AUR
for package in "${aur_packages[@]}"; do
    yay --sync --needed --noconfirm --norebuild --noredownload "$package" 1> /dev/null
done
