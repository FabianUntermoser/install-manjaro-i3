#!/usr/bin/env bash

# Install nvim
sudo pacman --sync --needed --noconfirm \
    neovim \
    python-pynvim \
    1> /dev/null

# Install pynvim for python3 nvim plugins
python3 -m pip install --user --upgrade pynvim

# install german spellfile
[ -f "/usr/share/nvim/runtime/spell/de.utf-8.spl" ] || {
    cd /usr/share/nvim/runtime/spell &&
        sudo wget http://ftp.vim.org/vim/runtime/spell/de.utf-8.spl
}
