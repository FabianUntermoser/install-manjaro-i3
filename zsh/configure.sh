#!/usr/bin/env bash

# fix issues in autocompleting
# force rebuild of zcompdump
zsh -ic "rm -f ~/.zcompdump; compinit"
