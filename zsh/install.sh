#!/usr/bin/env bash

# install packages
sudo pacman --sync --needed --noconfirm zsh 1> /dev/null

# setup xdg env
: ${XDG_CONFIG_HOME:="$HOME/.config"}
: ${XDG_CACHE_HOME:="$HOME/.cache"}
: ${XDG_DATA_HOME:="$HOME/.local/share"}

# create required folders
export ZDOTDIR="$XDG_CONFIG_HOME"/zsh # zsh
mkdir -p "$ZDOTDIR"
mkdir -p "$XDG_DATA_HOME/z"

# change shell to zsh
sudo chsh --shell "$(command -v zsh)" "$USER"
