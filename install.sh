#!/usr/bin/env bash

. "./lib.sh"

# default packages
packages=(
    zsh
    nvim
    dev-packages
    misc-packages
    shell-themes

    docker

    ## i3
    # i3exit

    ## ARCHIVE
    # vim
    # st
    # texlive
    # libreoffice
)

# overwrite packages with program arguments
if [ $# -gt 0 ]; then
    packages=("$@")
fi

ensure_privileges

# upgrade system
log "updating system"
sudo pacman --sync --refresh --sysupgrade --needed --noconfirm 1> /dev/null

for package in "${packages[@]}"; do

    test -f  "./$package/install.sh" && {
        log "installing $package .."
        ./"$package"/install.sh
    }

    test -f  "./$package/configure.sh" && {
        log "configuring $package .."
        ./"$package"/configure.sh
    }

done

log "FINISHED INSTALLING"

log "Now, create these symbolic links: ~/wallpaper, /Dev"
